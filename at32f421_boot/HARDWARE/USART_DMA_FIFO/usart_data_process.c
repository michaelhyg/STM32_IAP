//#include "usart_data_process.h"
//#include "FIFO.h"

//u16 time3_cnt = 0;
//u8 read_Flag = 0; //用于接收数据超时判断
//u8 data_len = 0;  //数据长度
//extern FIFOTYPE *uartFIFO;

//void TIM3_Int_Init(u16 arr, u16 psc)
//{
//    TMR_TimerBaseInitType TIM_TimeBaseInitStructure;
//    NVIC_InitType NVIC_InitStructure;
//    /* TMR3 clock enable */
//	RCC_APB1PeriphClockCmd(RCC_APB1PERIPH_TMR3, ENABLE);
//	
//	TIM_TimeBaseInitStructure.TMR_DIV = psc;                       //分频值
//    TIM_TimeBaseInitStructure.TMR_CounterMode = TMR_CounterDIR_Up; //计数模式
//    TIM_TimeBaseInitStructure.TMR_Period = arr;                    //自动重装数值
//    TIM_TimeBaseInitStructure.TMR_ClockDivision = TMR_CKD_DIV1;    //设置时钟分割
//	TMR_TimeBaseInit(TMR3, &TIM_TimeBaseInitStructure);
//	
//	/* TMR IT enable */
//    TMR_INTConfig(TMR3, TMR_INT_Overflow, ENABLE);

//	/* Enable the TMR2 global Interrupt */
//    NVIC_InitStructure.NVIC_IRQChannel = TMR3_GLOBAL_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	
//    NVIC_Init(&NVIC_InitStructure);

//    /* TMR2 enable counter */
//    TMR_Cmd(TMR3, ENABLE);
//}

//void TMR3_GLOBAL_IRQHandler(void) //TIM3中断
//{
//    if (TMR_GetINTStatus(TMR3, TMR_INT_Overflow) != RESET) //检查指定的TIM中断发生与否:TIM 中断源
//    {
//        TMR_ClearITPendingBit(TMR3, TMR_INT_Overflow); //清除TIMx的中断待处理位:TIM 中断源
//        time3_cnt++;
//        if (read_Flag) //超过时间还没有收到数据，则清零标志位
//        {
//            if ((data_len > FIFO_Status(uartFIFO)) && (time3_cnt >= 5)) //1M波特率，一个数据10us，超过500*10us=5ms则算超时，根据不同数据长度选择时间
//            {
//                read_Flag = 0;
//                //printf("ERROR DATA FIFO LEN = %d\r\n", FIFO_Status(uartFIFO));
//            }
//        }
//    }
//}

///*数据包处理*/
///*数据格式*/
///*头码55 55，长度包含除头码之外的所有字节*/
////  头码    |  长度  |  命令  |  数据  |  校验
////  55 55      03       01       01      无
//u8 usart1_Process(u8 *data, u16 *data_length)
//{
//    u8 read_Data;
//    if (read_Flag == 1) //收到头码，等待数据接收完成，超时则清零
//    {
//        if (FIFO_Status(uartFIFO) >= data_len)
//        {
//            /*处理数据*/
//            FIFO_ReadN(uartFIFO, data, data_len);
//            *data_length = data_len;
//            read_Flag = 0; //清除标志
//            return 0;
//        }
//    }
//    else
//    {
//        /*接收头码*/
//        if (FIFO_Status(uartFIFO) >= 6) //最小一帧数据6字节
//        {
//            read_Data = 0;
//            FIFO_Read(uartFIFO, &read_Data, 1);
//            if (read_Data != 0x55)
//            {
//                return 1;
//            }
//            else
//            {
//                read_Data = 0;
//                FIFO_Read(uartFIFO, &read_Data, 1);
//                if (read_Data != 0x55)
//                {
//                    return 1;
//                }
//                else
//                {
//                    data_len = 0;
//                    FIFO_Read(uartFIFO, &data_len, 0);
//                    if (FIFO_Status(uartFIFO) >= data_len)
//                    {
//                        /*处理数据*/
//                        FIFO_ReadN(uartFIFO, data, data_len);
//                        *data_length = data_len;
//                        read_Flag = 0; //清除标志
//                        return 0;
//                    }
//                    else
//                    {
//                        time3_cnt = 0;
//                        read_Flag = 1; //数据没有接收完成，标志置位，等待定时器中判断
//                        return 1;
//                    }
//                }
//            }
//        }
//    }
//	return 1;
//}

