/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-07-21 16:06:38
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-16 10:12:31
 */
#ifndef __IAP_H__
#define __IAP_H__
#include "sys.h"

typedef void (*iapfun)(void); //定义一个函数类型的参数.

#define FLASH_APP_ADDR 0x08003000 //应用程序起始地址(存放在FLASH)

void iap_load_app(u32 appxaddr);                             //执行flash里面的app程序
void iap_load_appsram(u32 appxaddr);                         //执行sram里面的app程序
void iap_write_appbin(u32 appxaddr, u8 *appbuf, u32 applen); //在指定地址开始,写入bin
#endif
