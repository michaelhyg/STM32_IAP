/*
 * @Descripttion:
 * @version:
 * @Author: MichaelHu
 * @Date: 2020-08-28 16:08:41
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-24 14:03:33
 */
#include "FreeRTOS.h"
#include "at32f4xx.h"
#include "delay.h"
#include "iap.h"
#include "sys.h"
#include "usart.h"

typedef enum
{
    START = 1,
    PICTURE_INFO,
    PICTURE_DATA,
    END,
} USART_CMD_TypeDef;

int main(void)
{
    Process_RES_TypeDef res;
    u8 *buf = pvPortMalloc(1200);
    u16 len;
    u16 t = 0;
    bool flag = false;
    USART_CMD_TypeDef cmd;

    enum PLLClkSrc ClkSrc = PLL_HSI;
    SysclkConfig(ClkSrc, 120);
    delay_init(120);
    usart_dma_init(115200);

    send_Serial_CMD(USART1, 11);
    u16 iap_write_cnt = 0;
    u32 total;
    for (;;)
    {
        res = data_Process_onebuf(uartFIFO, (u8 *)(&cmd), buf, &len);
        if (res == OK)
        {
            switch (cmd)
            {
            case START:
                flag = true;
                break;
            case PICTURE_INFO:
                total = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | (buf[3] << 0);
                iap_write_cnt = 0;
                printf("\r\ntotal=%d\r\n", total);
                send_Serial_CMD(USART1, 22);
                FLASH_Unlock();
                for (int i = 0; i < total / 1024 + 1; i++)
                {
                    /* ���� */
                    FLASH_ErasePage(FLASH_APP_ADDR + i * 1024);
                }
                break;
            case PICTURE_DATA:
                for (int i = 0; i < 1024 / 2; i++)
                {
                    u16 temp;
                    temp = buf[i * 2 + 1] << 8;
                    temp += buf[i * 2];
                    FLASH_ProgramHalfWord(FLASH_APP_ADDR + (iap_write_cnt * 1024) + i * 2, temp);
                }
                iap_write_cnt++;
                send_Serial_CMD(USART1, 22);
                break;
            case END:
                FLASH_Lock();                                                     //����
                if (((*(vu32 *)(FLASH_APP_ADDR + 4)) & 0xFF000000) == 0x08000000) //�ж��Ƿ�Ϊ0X08XXXXXX.
                {
                    iap_load_app(FLASH_APP_ADDR); //ִ��FLASH APP����
                }
                break;
            default:
                break;
            }
        }
        t++;
        if ((t >= 5) && (flag == false))
        {
            break;
        }
        delay_ms(10);
    }
    if (((*(vu32 *)(FLASH_APP_ADDR + 4)) & 0xFF000000) == 0x08000000) //�ж��Ƿ�Ϊ0X08XXXXXX.
    {
        iap_load_app(FLASH_APP_ADDR); //ִ��FLASH APP����
    }
}

#ifdef USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
