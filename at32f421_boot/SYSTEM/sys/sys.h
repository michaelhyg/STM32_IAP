/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-08-28 16:08:41
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-19 18:25:26
 */
#ifndef __SYS_H
#define __SYS_H
#include "at32f4xx.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//IO口操作宏定义
#define BITBAND(addr, bitnum) ((addr & 0xF0000000) + 0x2000000 + ((addr & 0xFFFFF) << 5) + (bitnum << 2))
#define MEM_ADDR(addr) *((volatile unsigned long *)(addr))
#define BIT_ADDR(addr, bitnum) MEM_ADDR(BITBAND(addr, bitnum))
//IO口地址映射
#define GPIOA_ODR_Addr (GPIOA_BASE + 20) //0x40020014
#define GPIOB_ODR_Addr (GPIOB_BASE + 20) //0x40020414
#define GPIOC_ODR_Addr (GPIOC_BASE + 20) //0x40020814
#define GPIOD_ODR_Addr (GPIOD_BASE + 20) //0x40020C14
#define GPIOE_ODR_Addr (GPIOE_BASE + 20) //0x40021014
#define GPIOF_ODR_Addr (GPIOF_BASE + 20) //0x40021414
#define GPIOG_ODR_Addr (GPIOG_BASE + 20) //0x40021814
#define GPIOH_ODR_Addr (GPIOH_BASE + 20) //0x40021C14
#define GPIOI_ODR_Addr (GPIOI_BASE + 20) //0x40022014

#define GPIOA_IDR_Addr (GPIOA_BASE + 32) //0x40020010
#define GPIOB_IDR_Addr (GPIOB_BASE + 32) //0x40020410
#define GPIOC_IDR_Addr (GPIOC_BASE + 32) //0x40020810
#define GPIOD_IDR_Addr (GPIOD_BASE + 32) //0x40020C10
#define GPIOE_IDR_Addr (GPIOE_BASE + 32) //0x40021010
#define GPIOF_IDR_Addr (GPIOF_BASE + 32) //0x40021410
#define GPIOG_IDR_Addr (GPIOG_BASE + 32) //0x40021810
#define GPIOH_IDR_Addr (GPIOH_BASE + 32) //0x40021C10
#define GPIOI_IDR_Addr (GPIOI_BASE + 32) //0x40022010

//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n) BIT_ADDR(GPIOA_ODR_Addr, n) //输出
#define PAin(n) BIT_ADDR(GPIOA_IDR_Addr, n)  //输入

#define PBout(n) BIT_ADDR(GPIOB_ODR_Addr, n) //输出
#define PBin(n) BIT_ADDR(GPIOB_IDR_Addr, n)  //输入

#define PCout(n) BIT_ADDR(GPIOC_ODR_Addr, n) //输出
#define PCin(n) BIT_ADDR(GPIOC_IDR_Addr, n)  //输入

#define PDout(n) BIT_ADDR(GPIOD_ODR_Addr, n) //输出
#define PDin(n) BIT_ADDR(GPIOD_IDR_Addr, n)  //输入

#define PEout(n) BIT_ADDR(GPIOE_ODR_Addr, n) //输出
#define PEin(n) BIT_ADDR(GPIOE_IDR_Addr, n)  //输入

#define PFout(n) BIT_ADDR(GPIOF_ODR_Addr, n) //输出
#define PFin(n) BIT_ADDR(GPIOF_IDR_Addr, n)  //输入

#define PGout(n) BIT_ADDR(GPIOG_ODR_Addr, n) //输出
#define PGin(n) BIT_ADDR(GPIOG_IDR_Addr, n)  //输入

#define PHout(n) BIT_ADDR(GPIOH_ODR_Addr, n) //输出
#define PHin(n) BIT_ADDR(GPIOH_IDR_Addr, n)  //输入

#define PIout(n) BIT_ADDR(GPIOI_ODR_Addr, n) //输出
#define PIin(n) BIT_ADDR(GPIOI_IDR_Addr, n)  //输入

/* Private typedef -----------------------------------------------------------*/
typedef struct ClockConfigStruct
{
    uint32_t Sysclk;
    uint32_t APB1_Div;
    uint32_t APB2_Div;
    uint32_t Mult;
    uint32_t PllRange;
} ClkConfStruct;

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
enum PLLClkSrc
{
    PLL_HSI = 0,
    PLL_HSE,
    PLL_end
};

static ClkConfStruct ClockTable[] =
{
        {24, RCC_CFG_APB1PSC_DIV1, RCC_CFG_APB2PSC_DIV1, RCC_CFG_PLLMULT6, RCC_CFG_PLLRANGE_LE72MHZ},
        {36, RCC_CFG_APB1PSC_DIV1, RCC_CFG_APB2PSC_DIV1, RCC_CFG_PLLMULT9, RCC_CFG_PLLRANGE_LE72MHZ},
        {48, RCC_CFG_APB1PSC_DIV1, RCC_CFG_APB2PSC_DIV1, RCC_CFG_PLLMULT12, RCC_CFG_PLLRANGE_LE72MHZ},
        {56, RCC_CFG_APB1PSC_DIV1, RCC_CFG_APB2PSC_DIV1, RCC_CFG_PLLMULT14, RCC_CFG_PLLRANGE_LE72MHZ},
        {72, RCC_CFG_APB1PSC_DIV1, RCC_CFG_APB2PSC_DIV1, RCC_CFG_PLLMULT18, RCC_CFG_PLLRANGE_LE72MHZ},
        {96, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT24, RCC_CFG_PLLRANGE_GT72MHZ},
        {108, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT27, RCC_CFG_PLLRANGE_GT72MHZ},
        {120, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT30, RCC_CFG_PLLRANGE_GT72MHZ},
        {144, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT36, RCC_CFG_PLLRANGE_GT72MHZ},
#if defined(AT32F403xx) || defined(AT32F413xx) || \
    defined(AT32F403Axx) || defined(AT32F407xx)
        {168, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT42, RCC_CFG_PLLRANGE_GT72MHZ},
        {176, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT44, RCC_CFG_PLLRANGE_GT72MHZ},
        {192, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT48, RCC_CFG_PLLRANGE_GT72MHZ},
        {200, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT50, RCC_CFG_PLLRANGE_GT72MHZ},
#endif
#if defined(AT32F403Axx) || defined(AT32F407xx)
        {224, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT56, RCC_CFG_PLLRANGE_GT72MHZ},
        {240, RCC_CFG_APB1PSC_DIV2, RCC_CFG_APB2PSC_DIV2, RCC_CFG_PLLMULT60, RCC_CFG_PLLRANGE_GT72MHZ}
#endif
};

ErrorStatus SysclkConfig(enum PLLClkSrc Src, uint32_t DestClkFreq);
void ClockReset(void);
#endif
