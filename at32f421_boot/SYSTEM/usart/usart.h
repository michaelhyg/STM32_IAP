#ifndef __USART_H
#define __USART_H
#include "FIFO.h"
#include "sys.h"
#include "easyflash.h"

#define FIFO_BUF_LENGTH 1200
#define FIFO_DMA 1 //采用DMA模式
#define uartDMA_CH DMA1_Channel3
#define uartCNDTR uartDMA_CH->TCNT

extern FIFOTYPE *uartFIFO;
extern u8 *process_buf;
extern bool uartFlag;
extern u16 tmr3_cnt;
typedef enum
{
    OK = 1,
    CRC_NG,
    BUF_LENGTH_ERR, //数据缓存长度不够
    NG,
} Process_RES_TypeDef;

void uart_init(u32 bound);
void send_Serial_CMD(USART_Type *USARTx, u8 cmd);
void send_Serial_Data(u8 cmd, u8 *buf, u16 length);
void usart_dma_init(u32 bound);
Process_RES_TypeDef data_Process(FIFOTYPE *fifo, u8 *cmd, u32 **data, u32 **secondData, u16 *secondLen, u16 *totalLen);
Process_RES_TypeDef data_Process_onebuf(FIFOTYPE *fifo, u8 *cmd, u8 *data, u16 *len);
void usart_respond(USART_Type *USARTx);
#endif
