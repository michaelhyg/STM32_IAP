#include "delay.h"
#include "iap.h"
#include "stmflash.h"
#include "sys.h"
#include "usart.h"
#include <stdlib.h>
#include <string.h>

extern int hpatchi(size_t patchCacheSize);
u8 *old;

int main(void)
{
    delay_init();      // 初始化延时函数
    uart_init(115200); // 初始化串口波特率为115200
    delay_ms(1000);    // 启动后适当延时用以接收串口数据
    old = (u8 *)(FLASH_APP_ADDR);
    u8 *p = (u8 *)(FLASH_APP_ADDR);
    sizeold = 13928;
    u32 oldsum = 0;
    u32 diffsum = 0;
    // if (((*(vu32 *)(FLASH_APP_ADDR + 4)) & 0xFF000000) == 0x08000000) // 判断是否为0X08XXXXXX.
    // {
    //     iap_load_app(FLASH_APP_ADDR); // 执行FLASH APP代码
    // }

    //		printf("%02X ", (u8)(old[i]>>0));
    //		printf("%02X ", (u8)(old[i]>>8));
    //		printf("%02X ", (u8)(old[i]>>16));
    //		printf("%02X ", (u8)(old[i]>>24));
    for (int i = 0; i < 13928; i++)
    {
        oldsum += old[i];
    }
    for (int i = 0; i < sizeof(diff); i++)
    {
        diffsum += diff[i];
    }

    printf("oldsum = %X diffsum = %X\r\n", oldsum, diffsum); 
	
    hpatchi(1024);
    while (1)
    {

        // delay_ms(10);
    }
}
