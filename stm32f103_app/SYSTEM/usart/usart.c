/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2020-09-17 11:14:21
 * @LastEditors: MichaelHu
 * @LastEditTime: 2021-06-22 18:06:20
 */
#include "usart.h"
#include "crc32.h"

//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数,而不需要选择use MicroLIB
#if 1
#pragma import(__use_no_semihosting)
//标准库需要的支持函数
struct __FILE
{
    int handle;
};

FILE __stdout;
//定义_sys_exit()以避免使用半主机模式
void _sys_exit(int x)
{
    x = x;
}
//重定义fputc函数
int fputc(int ch, FILE *f)
{
    while ((USART1->SR & 0X40) == 0) {}; //循环发送,直到发送完毕
    USART1->DR = (u8)ch;
    return ch;
}

void _ttywrch(int ch)
{
    ch = ch;
}
#endif

FIFOTYPE *uartFIFO;
static u16 time3_cnt = 0;
static u8 read_Flag = 0; //用于接收数据超时判断
static u32 data_len = 0;  //数据长度

void uart_init(u32 bound)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    //USART 初始化设置
    USART_InitStructure.USART_BaudRate = bound;                                     //串口波特率
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;                     //字长为8位数据格式
    USART_InitStructure.USART_StopBits = USART_StopBits_1;                          //一个停止位
    USART_InitStructure.USART_Parity = USART_Parity_No;                             //无奇偶校验位
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //无硬件数据流控制
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;                 //收发模式

    USART_Init(USART1, &USART_InitStructure);
    USART_Cmd(USART1, ENABLE);

#ifndef FIFO_DMA
    NVIC_InitTypeDef NVIC_InitStructure;
    //Usart1 NVIC 配置
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3; //抢占优先级3
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;        //子优先级3
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;           //IRQ通道使能
    NVIC_Init(&NVIC_InitStructure);                           //根据指定的参数初始化VIC寄存器
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);            //开启串口接受中断
#endif
}

#ifndef FIFO_DMA
void USART1_IRQHandler(void) //串口1中断服务程序
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) //接收到数据
    {
        uartFIFO->buffer[FIFO_BUF_LENGTH - uartFIFO->CNDTR] = USART_ReceiveData(USART1); //读取接收到的数据
        uartFIFO->CNDTR--;
        if (uartFIFO->CNDTR == 0)
        {
            uartFIFO->CNDTR = FIFO_BUF_LENGTH;
        }
    }
}
#endif

void MYDMA_Config(DMA_Channel_TypeDef *DMA_CHx, u32 cpar, u32 cmar, u16 cndtr)
{
    DMA_InitTypeDef DMA_InitStructure;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE); //使能DMA时钟

    DMA_DeInit(DMA_CHx);
    DMA_InitStructure.DMA_PeripheralBaseAddr = cpar;                 //外设地址
    DMA_InitStructure.DMA_MemoryBaseAddr = cmar;                     //内存地址
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;               //传输方向外设到内存
    DMA_InitStructure.DMA_BufferSize = cndtr;                        //传输量
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //外设地址不自增
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;          //内存地址自增
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;     //循环模式
    DMA_InitStructure.DMA_Priority = DMA_Priority_High; //高优先级
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA_CHx, &DMA_InitStructure);

    /*中断*/
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    //DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, ENABLE); //配置DMA发送完成后产生中断

    USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE); //允许DMA请求
    DMA_Cmd(DMA1_Channel5, ENABLE);
}

void DMA1_Channel5_IRQHandler(void)
{
    if (DMA_GetITStatus(DMA1_IT_TC5))
    {
        DMA_ClearITPendingBit(DMA1_IT_GL5);
        //DMA_TC_IT_flag++;
    }
}

static void TIM3_Int_Init(u16 arr, u16 psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    TIM_TimeBaseInitStructure.TIM_Prescaler = psc; //分频值
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Period = arr; //自动重装数值
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_Cmd(TIM3, ENABLE);
}

void usart_dma_init(u32 bound)
{
    uart_init(bound);
    //初始化FIFO
    FIFO_Init(&uartFIFO, FIFO_BUF_LENGTH);
    TIM3_Int_Init(1000 - 1, 72 - 1); //1MHZ 1000次  1ms计时
    MYDMA_Config(DMA1_Channel5, (u32)&USART1->DR, uartFIFO->staraddr, uartFIFO->size);
}

void TIM3_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
        time3_cnt++;
        if (read_Flag) //超过时间还没有收到数据，则清零标志位
        {
			//波特率460800＝ 460800 (位/秒) ＝ 46.08 (KB/秒)， 最大接收数据长度 = 5ms*46.08(B/ms)= 230.4
			//目前测试5ms时间，可以接收处理200字节数据，根据字节需要修改时间
            if ((data_len > FIFO_Status(uartFIFO, uartCNDTR)) && (time3_cnt >= 5)) 
            {
                read_Flag = 0;
            }
        }
    }
}

void send_Serial_CMD(u8 cmd)
{
    u16 len = 0;
    u32 crc;
	u8 send_buf[1024];

    send_buf[len++] = 0x55;
    send_buf[len++] = 0x55;
    send_buf[len++] = 0;   //长度高位
    send_buf[len++] = 0;   //长度低位
    send_buf[len++] = cmd; //CMD

    send_buf[2] = (u8)(len >> 8);
    send_buf[3] = (u8)(len >> 0);

    crc = 0;
    crc = calc_crc32(crc, &send_buf[4], len - 4);

    send_buf[len++] = (u8)(crc >> 24);
    send_buf[len++] = (u8)(crc >> 16);
    send_buf[len++] = (u8)(crc >> 8);
    send_buf[len++] = (u8)(crc >> 0);

    for (int i = 0; i < len; i++)
    {
        while ((USART1->SR & 0X40) == 0) {};
        USART1->DR = (u8)send_buf[i];
    }
}

/*数据格式*/
/*头码55 55，长度包含除头码之外的所有字节*/
//  头码    |  长度  |  命令  |  数据  |  校验
//  55 55      00 03     01       01
void send_Serial_Data(u8 cmd, u8 *buf, u16 length)
{
    u16 len = 0;
    u32 crc;
	u8 send_buf[1024];

    send_buf[len++] = 0x55;
    send_buf[len++] = 0x55;
    send_buf[len++] = 0;   //长度高位
    send_buf[len++] = 0;   //长度低位
    send_buf[len++] = cmd; //CMD

    for (int i = 0; i < length; i++)
    {
        send_buf[len++] = buf[i];
    }
    send_buf[2] = (u8)(len >> 8);
    send_buf[3] = (u8)(len >> 0);

    crc = 0;
    crc = calc_crc32(crc, &send_buf[4], len - 4);

    send_buf[len++] = (u8)(crc >> 24);
    send_buf[len++] = (u8)(crc >> 16);
    send_buf[len++] = (u8)(crc >> 8);
    send_buf[len++] = (u8)(crc >> 0);

    for (int i = 0; i < len; i++)
    {
        while ((USART1->SR & 0X40) == 0) {};
        USART1->DR = (u8)send_buf[i];
    }
}

/**
 * @description: 获取数据
 * @param {FIFOTYPE} *fifo 数据指针
 * @param {u8} *cmd 指令
 * @param {u32} **data 数据
 * @param {u32} ** secondData 数据循环接收完成后的环形接收头数据
 * @param {u16} *secondLen	第二段数据长度
 * @param {u16} *totalLen	总数据长度
 * @return {*}
 */
Process_RES_TypeDef data_Process(FIFOTYPE *fifo, u8 *cmd, u32 **data, u32 **secondData, u16 *secondLen, u16 *totalLen)
{
    u8 read_Data;
    u8 lenH, lenL;
    u32 crc;
    u8 crc_buf[4];
    static u8 processFlag = 0;
	static u32 read_len;
    if (read_Flag == 1) //收到头码，等待数据接收完成，超时则清零
    {
		read_len = FIFO_Status(fifo, uartCNDTR);
        if (read_len >= data_len)
        {
            processFlag = 1;
        }
    }
    else
    {
        /*接收头码*/
        if (FIFO_Status(fifo, uartCNDTR) >= 6) //最小一帧数据6字节
        {
            read_Data = 0;
            FIFO_Read(fifo, &read_Data, 1, uartCNDTR);
            if (read_Data != 0x55)
            {
                return NG;
            }
            else
            {
                read_Data = 0;
                FIFO_Read(fifo, &read_Data, 1, uartCNDTR);
                if (read_Data != 0x55)
                {
                    return NG;
                }
                else
                {
                    data_len = 0;
                    FIFO_Read(fifo, &lenH, 1, uartCNDTR);
                    FIFO_Read(fifo, &lenL, 1, uartCNDTR);
                    data_len = (lenH << 8) + lenL;
					
					if((data_len - 9) > FIFO_BUF_LENGTH)	//数据长度大于整个BUF的大小则返回错误
					{
						
					}
					
                    if (FIFO_Status(fifo, uartCNDTR) >= data_len)
                    {
                        processFlag = 1;
                    }
                    else
                    {
                        time3_cnt = 0;
                        read_Flag = 1; //数据没有接收完成，标志置位，等待定时器中判断
                        return NG;
                    }
                }
            }
        }
    }
    if (processFlag)
    {
        processFlag = 0;
        /*处理数据*/
        FIFO_Read(fifo, cmd, 1, uartCNDTR);
        *totalLen = data_len - 5;
        read_Flag = 0; //清除标志

        u32 surplus;
        surplus = ((u32)(fifo->endaddr) + 1 - (u32)(fifo->front));
        if (surplus < *totalLen)
        {
            *data = ((u32 *)(fifo->front));
            *secondLen = *totalLen - surplus;
            *secondData = ((u32 *)(fifo->staraddr));
        }
        else
        {
            *secondLen = 0;
            *data = ((u32 *)(fifo->front));
        }

        crc = 0;
        crc = calc_crc32(crc, cmd, 1);
        crc = calc_crc32(crc, ((u8 *)(fifo->front)), *totalLen - *secondLen);
        crc = calc_crc32(crc, ((u8 *)(fifo->staraddr)), *secondLen);

        FIFO_Remove(fifo, data_len - 1 - 4);
        if (FIFO_Status(fifo, uartCNDTR) >= 4)
        {
            //读取CRC
            FIFO_ReadN(fifo, crc_buf, 4, uartCNDTR);
        }
        u32 c = (u32)crc_buf[0] << 24 | (u32)crc_buf[1] << 16 | (u32)crc_buf[2] << 8 | (u32)crc_buf[3] << 0;
        if (crc == c)
        {
            return OK;
        }
        else
        {
            return CRC_NG;
        }
    }
    return NG;
}

