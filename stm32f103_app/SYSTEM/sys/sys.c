#include "sys.h"

/*********************************************************************************
  * @函数名：Stm32_Clock_Init
  * @描  述：系统时钟初始化函数
  * @参  数：CLKFROM  选择时钟来源，CLK_HSI 内部时钟默认4M，CLK_HSE 外部时钟
  *                clk      选择的时钟频率 1M 8M 72M。。。                
  * @返回值；无
  * @说  明：
**********************************************************************************/ 
void Stm32_Clock_Init(u8 CLKFROM, u8 clk)
{
	unsigned char temp = 0; 
	
	u8 PLL;
	//内部时钟
	if(CLKFROM == CLK_HSI)
	{
		PLL = clk/4;
	}
	else if(CLKFROM == CLK_HSE)
	{
		PLL = clk/8;		//外部晶振默认认为是8M晶振。
	}
	
	RCC_DeInit();                            // 复位并配置向量表

	RCC->CFGR = 0X00000400;     // APB1 = DIV2; APB2 = DIV1; AHB = DIV1;
	PLL -= 2;                                            // 抵消2个单位（因为是从2开始的，设置0就是2）
	RCC->CFGR |= PLL << 18;     // 设置PLL值 2~16

	if(CLK_HSI == CLKFROM)      // 使用内部时钟  SYSCLK = 4 * PLL MHz
	{
		RCC->CR |= 0x00000001;    // 内部高速时钟使能HSEON
		while(!(RCC->CR >> 1));   // 等待内部时钟就绪
		RCC->CFGR &= ~(1 << 16);        // HSI振荡器时钟经2分频后作为PLL输入时钟
	}
	else if(CLK_HSE == CLKFROM) // 使用外部时钟 SYSCLK = HSE * PLL MHz
	{
		RCC->CR |= 0x00010000;    // 外部高速时钟使能HSEON
		while(!(RCC->CR >> 17));  // 等待外部时钟就绪
		RCC->CFGR |= 1 << 16;            // HSE时钟作为PLL输入时钟 
	}

	FLASH->ACR |= 0x32;                // FLASH 2个延时周期
	RCC->CR |= 0x01000000;      // PLLON
	while(!(RCC->CR >> 25));    // 等待PLL锁定
	RCC->CFGR |= 0x00000002;    // PLL作为系统时钟         
	while(temp != 0x02)         // 等待PLL作为系统时钟设置成功
	{   
		temp = RCC->CFGR >> 2;
		temp &= 0x03;
	}    
} 

void WFI_SET(void)
{
	__ASM volatile("wfi");		  
}
//关闭所有中断
void INTX_DISABLE(void)
{		  
	__ASM volatile("cpsid i");
}
//开启所有中断
void INTX_ENABLE(void)
{
	__ASM volatile("cpsie i");		  
}
//开启所有中断
void NOP(void)
{
	__ASM volatile("nop");		  
}
//设置栈顶地址
//addr:栈顶地址
__asm void MSR_MSP(u32 addr) 
{
    MSR MSP, r0 			//set Main Stack value
    BX r14
}
