/*
 *                        _oo0oo_
 *                       o8888888o
 *                       88" . "88
 *                       (| -_- |)
 *                       0\  =  /0
 *                     ___/`---'\___
 *                   .' \\|     |// '.
 *                  / \\|||  :  |||// \
 *                 / _||||| -:- |||||- \
 *                |   | \\\  - /// |   |
 *                | \_|  ''\---/''  |_/ |
 *                \  .-\__  '-'  ___/-. /
 *              ___'. .'  /--.--\  `. .'___
 *           ."" '<  `.___\_<|>_/___.' >' "".
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /
 *      =====`-.____`.___ \_____/___.-`___.-'=====
 *                        `=---='
 * 
 * 
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 *            佛祖保佑       永不宕机     永无BUG
 */
/*
 * @Descripttion:
 * @version:
 * @Author: MichaelHu
 * @Date: 2019-12-23 14:26:20
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-11-23 18:55:19
 */

#include "FIFO.h"
#include "FreeRTOS.h"
#include "delay.h"
#include "easyflash.h"
#include "io.h"
#include "task.h"
#include "usart.h"

TaskHandle_t MAIN_Task_Handler;
void MAIN_task(void *pvParameters);

#define task1_PRIO 3
#define task1_SIZE 128
TaskHandle_t task1_Handler;
void task1(void *pvParameters);


int main(void)
{
	SCB->VTOR = FLASH_BASE | 0x3000;
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); //设置系统中断优先级分组4
    delay_init();
    IO_init();
    usart_dma_init(115200);

    taskENTER_CRITICAL(); 

    xTaskCreate((TaskFunction_t)MAIN_task, (const char *)"MAIN_task",
                (uint16_t)500, 
				(void *)NULL,
                (UBaseType_t)4, 
				(TaskHandle_t *)&MAIN_Task_Handler);
				
	xTaskCreate((TaskFunction_t)task1, (const char *)"task1",
                (uint16_t)500, 
				(void *)NULL,
                (UBaseType_t)5, 
				(TaskHandle_t *)&task1_Handler);

    taskEXIT_CRITICAL(); 

    vTaskStartScheduler(); 
}

void task1(void *pvParameters)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
	while (1)
	{
		PCout(13) = !PCout(13);
		vTaskDelay(200);
	}
}

void MAIN_task(void *pvParameters)
{
	u16 cnt = 0;
    while (1)
    {
        printf("hello world!!!  app2 cnt = %d\r\n", cnt++);
        vTaskDelay(500);
    }
}
