//#include "timer.h"
//#include "usart.h"

//u16 time3_cnt = 0;

//void TIM4_Int_Init(void)
//{
//    TIM_TimeBaseInitTypeDef	TIM_TimeBaseInitStructure;

//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);//开启TIM3时钟 

//	TIM_TimeBaseInitStructure.TIM_Prescaler= 36000 - 1;   //分频值
//	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;	   //计数模式
//	TIM_TimeBaseInitStructure.TIM_Period=65535;		   //自动重装数值
//	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;  //设置时钟分割
//	TIM_TimeBaseInit(TIM4,&TIM_TimeBaseInitStructure);
//	//TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);//允许更新中断

////	NVIC_InitStructure.NVIC_IRQChannel=TIM2_IRQn;
////	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
////	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
////	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
////	NVIC_Init(&NVIC_InitStructure);

//	TIM_Cmd(TIM4,ENABLE);		  //使能TIM2
//}



//void TIM3_Int_Init(u16 arr,u16 psc)
//{
//    TIM_TimeBaseInitTypeDef	TIM_TimeBaseInitStructure;
//	NVIC_InitTypeDef NVIC_InitStructure;
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);//开启TIM3时钟 

//	TIM_TimeBaseInitStructure.TIM_Prescaler=psc;   //分频值
//	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;	   //计数模式
//	TIM_TimeBaseInitStructure.TIM_Period=arr;		   //自动重装数值
//	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;  //设置时钟分割
//	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);
//	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);//允许更新中断

//	NVIC_InitStructure.NVIC_IRQChannel=TIM3_IRQn;
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority=3;
//	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
//	NVIC_Init(&NVIC_InitStructure);

//	TIM_Cmd(TIM3,ENABLE);		  //使能TIM3
//}

//void TIM3_IRQHandler(void)   //TIM3中断
//{
//	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源
//	{
//		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源
//		time3_cnt++;
//	}
//}

