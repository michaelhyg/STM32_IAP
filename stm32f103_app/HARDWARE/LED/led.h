/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2019-12-23 14:26:17
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-08-26 13:42:34
 */
#ifndef __LED_H
#define __LED_H
#include "sys.h"

#define LED PCout(13)

void LED_Init(void);

#endif
