/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2019-12-23 14:26:17
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-08-26 13:49:27
 */
#include "led.h"

void LED_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	LED = 0;
}
