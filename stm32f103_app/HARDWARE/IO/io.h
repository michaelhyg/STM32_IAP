/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2019-12-23 14:26:17
 * @LastEditors: MichaelHu
 * @LastEditTime: 2020-08-26 14:32:28
 */
#ifndef __IO_H
#define __IO_H
#include "sys.h"

#define BEEP PBout(10)

void IO_init(void);

#endif
