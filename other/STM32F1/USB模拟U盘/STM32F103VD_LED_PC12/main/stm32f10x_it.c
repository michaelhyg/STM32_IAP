/**
  ******************************************************************************
  * @file    stm32f10x_it.c 
  * @author  
  * @version V2.1
  * @date    03/12/2010
  * @brief  
  *
  *
  ******************************************************************************
  * 
  *
  *
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h" 
#include "usb_pwr.h"
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_istr.h"


/** @addtogroup Template_Project
  * @{
  */ 
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval : None
  */
void NMI_Handler(void)
{
	/* 时钟失效中断 */	
	if (RCC_GetITStatus(RCC_IT_CSS) != RESET) 
	{  
		/* HSE、PLL已被禁止(但是PLL设置未变) */
		/* 客户添加相应的系统保护代码处	*/
		
		//…… 
		 		
		/* 下面为HSE恢复后的预设置代码 */
		/* 使能HSE */
		RCC_HSEConfig(RCC_HSE_ON);  
		/* 使能HSE就绪中断 */
		RCC_ITConfig(RCC_IT_HSERDY, ENABLE); 
		/* 使能PLL就绪中断 */
		//RCC_ITConfig(RCC_IT_PLLRDY, ENABLE); 
		/* 清除时钟安全系统中断的挂起位	*/
		RCC_ClearITPendingBit(RCC_IT_CSS);  
		/* 至此，一旦HSE时钟恢复，将发生HSERDY中断，在RCC中断处理程序里，
		   系统时钟可以设置到以前的状态 */
	}

	/* HSE就绪中断 */
	if(RCC_GetITStatus(RCC_IT_HSERDY) != RESET)
	{
		/* 重新配置系统时钟 */
		SystemInit();
		/* 清除HSE就绪中断的挂起位	*/
		RCC_ClearITPendingBit(RCC_IT_HSERDY);
	}
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval : None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval : None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval : None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval : None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval : None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval : None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval : None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval : None
  */
void SysTick_Handler(void)
{

}


/*****************************************************************************
*
*串口接收中断	  目前设置最长数据1024
*
*****************************************************************************/

void USART1_IRQHandler (void)
{
    uint8_t c;
    
   	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
	    c = USART1->DR; 				
	} 
}
/*****************************************************************************
*
*DMA接收完成中断
*
*****************************************************************************/

void DMA1_Channel5_IRQHandler(void)
{
	(*((u32 *)0x40020004))=0xfffffff; 		/* 清除DMA   GIF TCIF 寄存器 */ 
	(*((u32 *)0x40020058))&=~(1<<0);  		/* 关闭DMA传输 */ 
}
/******************************************************************************
*	
*
*
*******************************************************************************/
void TIM3_IRQHandler(void) 
{
	u16 capture;	 
	TIM_ClearITPendingBit(TIM3, TIM_IT_CC1);
	capture = TIM_GetCapture1(TIM3);
        TIM_SetCompare1(TIM3, capture + 49152);			
	
}

/******************************************************************************
*	
*
*
******************************************************************************/
extern vu8 is_usb_connect; 

void TIM2_IRQHandler(void) 	  
{
	u32 capture;

	TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);
	capture = TIM_GetCapture1(TIM2);
        TIM_SetCompare1(TIM2, capture + 49152);       
}
/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval : None
  */
/*void PPP_IRQHandler(void)
{
}*/

/*******************************************************************************
* Function Name  : USB_HP_CAN1_TX_IRQHandler
* Description    : This function handles USB High Priority or CAN TX interrupts requests
*                  requests.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void USB_HP_CAN1_TX_IRQHandler(void)
{
  CTR_HP();
}
/*******************************************************************************
* Function Name  : USB_IRQHandler
* Description    : This function handles USB Low Priority interrupts
*                  requests.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/

void USB_LP_CAN1_RX0_IRQHandler(void)
{
  USB_Istr();
}
/*******************************************************************************
* Function Name  : USB_FS_WKUP_IRQHandler
* Description    : This function handles USB WakeUp interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/

void USBWakeUp_IRQHandler(void)
{
  EXTI_ClearITPendingBit(EXTI_Line18);

}
/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
