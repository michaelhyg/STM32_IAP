package my_interface

import (
	"fmt"
	"log"
	"os"
	"time"
)

func init() {
	_, err := os.Stat("mylog")
	if err == nil {
		fmt.Println(err)
	}
	if os.IsNotExist(err) {
		// 创建文件夹
		err := os.MkdirAll("mylog", os.ModePerm)
		if err != nil {
			fmt.Println(err)
		}
	}

	//fmt.Println("log init")
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	t := time.Unix(time.Now().Unix(), 0).Format("2006-01-02")
	t = t + ".log"
	t = "mylog/" + t
	logFile, err := os.OpenFile(t, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.Panic("打开日志文件异常")
	}
	log.SetOutput(logFile)
}
